export interface Contact {
  userId?: string;
  email: string;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  userName: string;
}
