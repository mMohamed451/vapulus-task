import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from './contact';
@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private http: HttpClient) { }

  getContactList() {
    return this.http.get<Contact[]>('http://localhost:3000/data');
  }
  addNewContact(contact: Contact) {
    return this.http.post('http://localhost:3000/data', contact);
  }
}
