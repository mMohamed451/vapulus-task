import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactsService } from '../contacts.service';
import { v4 as uuid } from 'uuid';
import { Contact } from '../contact';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  form: FormGroup;
  invalidForm: Boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private contactsService: ContactsService,
    private router: Router,

  ) {
    this.form = this.formBuilder.group({
      firstName: [
        '', [Validators.required, Validators.min(3)] // according to task requirement we add validation
      ],
      lastName: [
        '', [Validators.required, Validators.min(3)] // according to task requirement we add validation
      ],
      countryCode: [
        '', [Validators.required] // according to task requirement we add validation
      ],
      mobileNumber: [
        '', [Validators.required, Validators.min(3)] // according to task requirement we add validation
      ],
      email: [
        '', [Validators.required, Validators.min(3)] // according to task requirement we add validation
      ],
      imageName: [
        null, [Validators.required] // according to task requirement we add validation
      ],
    });
  }

  ngOnInit() {
  }
  onSubmit() {
    if (this.form.valid) {
      let newContact: any = {};
      this.form.value.userId = uuid(); // this step must be done by the backend not in the frontend.
      this.form.value.mobileNumber = this.form.value.countryCode + this.form.value.mobileNumber;
      newContact = this.form.value;
      delete newContact.countryCode;
      this.contactsService.addNewContact(newContact as Contact).subscribe((callback) => {
        this.router.navigate(['/contacts']);
      }, error => {
        console.log(error);
      });
    } else {
      this.invalidForm = true;
    }
  }

  onUploadPhoto(event: Event) { // we need bucket or drive to upload the photo to.
    // to save time i will take only the name of the photo.
    const file = (event.target as HTMLInputElement).files[0].name;
    this.form.patchValue({ imageName: file });
    this.form.get('imageName').updateValueAndValidity();
  }
}
