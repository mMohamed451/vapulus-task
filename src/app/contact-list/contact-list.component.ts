import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../contacts.service';
import { Contact } from '../contact';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  contacts: Contact[];
  window: Window = window;

  constructor(private contactService: ContactsService) {

  }

  ngOnInit() {
    this.contactService.getContactList().subscribe((contacts: Contact[]) => {
      this.contacts = contacts;
      console.log(this.contacts);
    }, error => {
      console.error('couldn"t get the data');
    });
  }

  onChange() {
    console.log('changed');
  }

}

