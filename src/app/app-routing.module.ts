import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddContactComponent } from './add-contact/add-contact.component';
import { ContactListComponent } from './contact-list/contact-list.component';


const routes: Routes = [{
  component: AddContactComponent, path: 'contacts/add-contact'
},
{
  component: ContactListComponent, path: 'contacts'
},
{
  redirectTo: 'contacts', path: '**'
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
